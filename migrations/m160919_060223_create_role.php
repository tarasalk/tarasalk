<?php

use yii\db\Migration;

class m160919_060223_create_role extends Migration
{
    public function up()
    {
        $this->createTable('role', [
            'id' => $this->primaryKey(),
            'name' => $this->string(15)->notNull(),
            'label' => $this->string(30)->notNull()
        ]);

        $this->insert('role', [
            'id' => 1,
            'name' => 'admin',
            'label' => 'админ'
        ]);
    }

    public function down()
    {
        echo "m160919_060223_create_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
