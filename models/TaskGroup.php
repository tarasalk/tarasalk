<?php

namespace app\models;

use app\models\activeRecord\Task;

class TaskGroup {

    const GROUP_FREE = 'Свободные';
    const GROUP_YEAR = 'Год';
    const GROUP_MONTH = 'Месяц';
    const GROUP_WEEK = 'Неделя';
    const GROUP_DAY = 'День';
    const GROUP_EXPIRED = 'Просроченные';

    protected $title;
    protected $tasks = [];

    public function __construct($title) {
        $this->title = $title;
    }

    public function addTask(array $aTask) {
        $this->tasks[] = $aTask;
    }

    public function addTaskRange(array $aTasks) {
        $this->tasks = array_merge($this->tasks, $aTasks);
    }

    public function getTitle() {
        return $this->title;
    }

    public function getTasks() {
        return $this->tasks;
    }

    public static function checkTaskGroup($aTask) {
        if ($aTask['period_type'] == Task::PERIOD_TYPE_PERIOD) {
            $dateCreateUnix = strtotime($aTask['date_create']);
            $periodSecond = $aTask['period_time'] * 24 * 60 * 60;

            $dateDeadlineUnix = $dateCreateUnix + $periodSecond;

            if (date('d.m.Y', time()) > date('d.m.Y', $dateDeadlineUnix)) {
                return self::GROUP_EXPIRED;
            }
        } else {
            if ($aTask['date_deadline'] == '0000-00-00 00:00:00') {
                return self::GROUP_FREE;
            }

            $dateDeadlineUnix = strtotime($aTask['date_deadline']);

            if (time() > $dateDeadlineUnix) {
                return self::GROUP_EXPIRED;
            }
        }

        if (date('d.m.Y', $dateDeadlineUnix) == date('d.m.Y')) {
            return self::GROUP_DAY;
        }

        if (date('W.Y', $dateDeadlineUnix) == date('W.Y')) {
            return self::GROUP_WEEK;
        }

        if (date('m.Y', $dateDeadlineUnix) == date('m.Y')) {
            return self::GROUP_MONTH;
        }

        if (date('Y', $dateDeadlineUnix) == date('Y')) {
            return self::GROUP_YEAR;
        }

        return false;
    }
}