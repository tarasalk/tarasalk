<?php

namespace app\controllers\cabinet;

use yii;
use app\helpers\JsonWrapper;
use app\models\activeRecord\Task;

class TimeController extends CabinetController {

    public function actionTaskboard() {
        $this->view->title = 'Доска задач';

        $aGroupedTasks = Task::getGroupedTasks();

        return $this->render('taskboard', compact('aGroupedTasks'));
    }
    
    /* ---------------------------- AJAX ---------------------------- */

    public function actionAddtask() {
        if (!Yii::$app->request->isAjax) {
            return JsonWrapper::error('ajax_only');
        }

        $title = Yii::$app->request->post('title');
        $description = Yii::$app->request->post('description');
        $dateDeadline = Yii::$app->request->post('dateDeadline');
        $periodType = Yii::$app->request->post('periodType');
        $periodTime = Yii::$app->request->post('periodTime');

        if (empty($title)) {
            return JsonWrapper::error('wrong_params');
        }

        $oTask = new Task;

        $oTask->title = $title;
        $oTask->description = $description;
        $oTask->period_type = $periodType;

        if ($periodType == Task::PERIOD_TYPE_PERIOD) {
            if (is_numeric($periodTime)) {
                $oTask->period_time = $periodTime;
            }
            else {
                return JsonWrapper::error('need_period_time');
            }
        }
        else {
            $oTask->date_deadline = $dateDeadline;
        }

        if ($oTask->save()) {
            return JsonWrapper::success();
        }

        return JsonWrapper::error();
    }

    public function actionGettask() {
        if (!Yii::$app->request->isAjax) {
            return JsonWrapper::error('ajax_only');
        }

        $task_id = Yii::$app->request->post('taskId');

        if (empty($task_id)) {
            return JsonWrapper::error('wrong_params');
        }

        $oTask = Task::findOne($task_id);
        if ($oTask) {
            return JsonWrapper::success($oTask->toArray());
        }

        return JsonWrapper::error();
    }

    public function actionUpdatetask() {
        if (!Yii::$app->request->isAjax) {
            return JsonWrapper::error('ajax_only');
        }

        $task_id = Yii::$app->request->post('taskId');
        $title = Yii::$app->request->post('title');
        $description = Yii::$app->request->post('description');
        $dateDeadline = Yii::$app->request->post('dateDeadline');

        if (empty($task_id) || empty($title)) {
            return JsonWrapper::error('wrong_params');
        }
        
        $oTask = Task::findOne($task_id);
        if ($oTask) {
            $oTask->title = $title;
            $oTask->description = $description;
            $oTask->date_deadline = $dateDeadline;

            if ($oTask->save()) {
                return JsonWrapper::success();
            }
        }

        return JsonWrapper::error();
    }

    public function actionDeletetask() {
        if (!Yii::$app->request->isAjax) {
            return JsonWrapper::error('ajax_only');
        }

        $task_id = Yii::$app->request->post('taskId');

        if (empty($task_id)) {
            return JsonWrapper::error('wrong_params');
        }

        if (Task::deleteAll((['id' => $task_id]))) {
            return JsonWrapper::success();
        }

        return JsonWrapper::error();
    }

    public function actionArchivingtask() {
        if (!Yii::$app->request->isAjax) {
            return JsonWrapper::error('ajax_only');
        }

        $task_id = Yii::$app->request->post('taskId');

        if (empty($task_id)) {
            return JsonWrapper::error('wrong_params');
        }

        $oTask = Task::findOne($task_id);
        if ($oTask) {
            $oTask->status = Task::STATUS_ARCHIVE;

            if ($oTask->save()) {
                return JsonWrapper::success();
            }
        }

        return JsonWrapper::error();
    }
    
    public function actionClosetask() {
        if (!Yii::$app->request->isAjax) {
            return JsonWrapper::error('ajax_only');
        }

        $task_id = Yii::$app->request->post('taskId');

        if (empty($task_id)) {
            return JsonWrapper::error('wrong_params');
        }

        $oTask = Task::findOne($task_id);
        if ($oTask) {
            $oTask->status = Task::STATUS_CLOSE;
            $oTask->date_close = date('Y-m-d H:i:s');

            if ($oTask->save()) {
                return JsonWrapper::success();
            }
        }

        return JsonWrapper::error();
    }

    /**
     * Закрыть периодическую задачу и повторить
     */
    public function actionRepeattask() {
        if (!Yii::$app->request->isAjax) {
            return JsonWrapper::error('ajax_only');
        }

        $task_id = Yii::$app->request->post('taskId');

        if (empty($task_id)) {
            return JsonWrapper::error('wrong_params');
        }

        /** @var $oTask Task*/
        $oTask = Task::findOne($task_id);
        if ($oTask) {
            $oTask->status = Task::STATUS_CLOSE;
            $oTask->date_close = date('Y-m-d H:i:s');

            if ($oTask->save()) {
                $oRepeatTask = new Task;

                $oRepeatTask->title = $oTask->title;
                $oRepeatTask->description = $oTask->description;
                $oRepeatTask->period_type = $oTask->period_type;
                $oRepeatTask->period_time = $oTask->period_time;

                if ($oRepeatTask->save()) {
                    return JsonWrapper::success();
                }
            }
        }

        return JsonWrapper::error();
    }
}
