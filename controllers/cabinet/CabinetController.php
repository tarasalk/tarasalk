<?php

namespace app\controllers\cabinet;

use yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class CabinetController extends Controller {

    public $layout = 'cabinet';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }
}
