function classes_AddRemove(searchElements, addClass, parent) {
    for (var i = 0; i < searchElements.length; i++) {
        var currentElement = searchElements[i] + " a";

        $(currentElement).each(function () {
            issetLink = this.href != '';
            hrefContainLink = window.location.href.search(this.href) != -1;

            // чтобы главная не активировалась всегда т.к. содержится во всех других ссылках
            wtf = (window.location.pathname != '/' && this.href != (window.location.origin + '/')) || window.location.pathname == '/';

            if (issetLink && hrefContainLink && wtf) {
                var workClass = parent ? $(this).parent() : $(this);

                workClass.addClass(addClass);
            }
        });
    }
}

function closeModal(thiselement, event) {
    if (event !== undefined) {
        if (event.target == thiselement) {
            $(thiselement).hide();
        }
    }
    else {
        $(thiselement).closest(".modal-overlay").hide();
    }
}

function openModalById(id) {
    $('#' + id).show();
}