<?php

use yii\db\Migration;

class m160919_060344_create_task extends Migration
{
    public function up()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'title' => $this->string(200)->notNull(),
            'description' => $this->text()->notNull(),
            'status' => $this->smallInteger()->defaultValue(0),
            'period_type' => $this->smallInteger()->defaultValue(0),
            'period_time' => $this->integer()->defaultValue(0),
            'date_create' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
            'date_deadline' => $this->dateTime()->notNull(),
            'date_close' => $this->dateTime()->notNull(),
        ]);
    }

    public function down()
    {
        echo "m160919_060344_create_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
