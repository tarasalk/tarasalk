<?php

namespace app\controllers;

use app\models\activeRecord\UserRole;
use app\models\Registration;
use app\models\User;
use OAuth\Common\Exception\Exception;
use yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SiteController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','about'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['about'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return UserRole::findOne(['user_id' => Yii::$app->user->id, 'role_id' => User::ROLE_ADMIN]);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {

        return $this->render('index');
    }

    public function actionLogin() {
        $serviceName = Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {

                    $oUser = User::findByEAuth($eauth);

                    if ($oUser == User::ERROR_USER_NOT_FOUND) {
                        $oRegistration = new Registration($eauth);
                        $oRegistration->save();

                        $oUser = User::findByEAuth($eauth);
                    }

                    if ($oUser instanceof User) {
                        Yii::$app->getUser()->login($oUser, 3600 * 24 * 30);
                    }
                    else {
                        throw new Exception($oUser);
                    }

                    $eauth->redirect();
                } else {
                    $eauth->cancel();
                }
            } catch (\nodge\eauth\ErrorException $e) {
                Yii::$app->getSession()->setFlash('error', 'EAuthException: ' . $e->getMessage());

                $eauth->cancel();
            }
        }

        // TODO по идеи нужно выводить сообщение что нужно авторизоваться
        // TODO пока need_auth только для наглядности, нигде не используется
        $this->redirect('/?need_auth');
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }
}
