<?php

namespace app\models\activeRecord;

use yii\db\ActiveRecord;


class UserOauth extends ActiveRecord {

    public static function tableName()
    {
        return 'user_oauth';
    }
}