<?php

namespace app\models\activeRecord;

use yii\db\ActiveRecord;


class UserRole extends ActiveRecord {

    public static function tableName()
    {
        return 'user_role';
    }
}