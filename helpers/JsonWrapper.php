<?php

namespace app\helpers;


class JsonWrapper {
	const SUCCESS = 'ok';
	const ERROR = 'error';

	public static function success($content = []) {
        $response = [
            'status' => JsonWrapper::SUCCESS,
            'content' => $content
        ];

        return self::response($response);
	}

	public static function error($content = 'undefined_error') {
		$response = [
			'status' => JsonWrapper::ERROR,
			'error' => $content
		];

        return self::response($response);
	}

    private static function response(array $response) {
        return json_encode($response);
    }
}