<?php

use yii\db\Migration;

class m160919_061122_create_user_role extends Migration
{
    public function up()
    {
        $this->createTable('user_role', [
            'user_id' => $this->integer()->notNull()->unique(),
            'role_id' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        echo "m160919_061122_create_user_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
