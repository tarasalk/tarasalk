<?php

/* @var $this yii\web\View */
/* @var $aMenu array */
?>

<style>
    .menu-level2 {
        display: none;
    }
</style>

<script>
    var $menuLevel2;

    $(function() {
        $menuLevel2 = $('.menu-level2');
    });

    function showMenuLevel2(title) {
        $menuLevel2.hide();
        $menuLevel2.filter('[data-title="'+title+'"]').show();
    }

    function hideMenuLevel2() {
        $menuLevel2.hide();
    }
</script>


<div class="menu-level1">
    <? foreach ($aMenu as $item) { ?>
        <a class="item" <?=isset($item['level2']) ? 'onclick="showMenuLevel2(\''.$item['title'].'\')"':'href="'.$item['link'].'"'?>>
            <div class="icon <?=$item['class']?>"></div>
            <div class="title">
                <?= $item['title']?>
            </div>
        </a>
    <? } ?>
</div>

<? foreach ($aMenu as $key => $item) { ?>
    <? if (isset($item['level2'])) { ?>
        <div class="menu-level2" data-title="<?=$item['title']?>">
            <div class="title">
                <div><?=$item['title']?></div>
                <div class="close" onclick="hideMenuLevel2()">&times;</div>
            </div>
            <? foreach ($item['level2'] as $item2) { ?>
                <a href="<?=$item2['link']?>" class="item"><?=$item2['title']?></a>
            <? } ?>
        </div>
    <? } ?>
<? } ?>
