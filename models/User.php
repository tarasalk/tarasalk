<?php

namespace app\models;

use Yii;
use app\models\activeRecord\UserOauth;
use nodge\eauth\ServiceBase;
use OAuth\Common\Exception\Exception;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface {

    public function getOauth() {
        return $this->hasOne(UserOauth::class, ['user_id' => 'id'])->alias('oauth');
    }

    /* -------------------------------------------------- */

    const ROLE_ADMIN = 1;

    const ERROR_USER_NOT_FOUND = 'user_not_found';
    const ERROR_UNDEFINED_OAUTH = 'undefined_oauth';

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {}

    public function validateAuthKey($authKey) {}

    public static function findIdentity($user_id) {
        return self::findOne($user_id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new Exception('accessToken not supported');
    }

    public static function findByVkId($vk_id) {
        return self::find()->joinWith('oauth')->where(['oauth.vk_id' => $vk_id])->one();
    }

    /**
     * может вынести в модель авторизации?
     * @param \nodge\eauth\ServiceBase $service
     * @return User
     * @throws \ErrorException
     */
    public static function findByEAuth(ServiceBase $service) {
        if (!$service->getIsAuthenticated()) {
            throw new \ErrorException('EAuth user should be authenticated before creating identity.');
        }

        switch ($service->getServiceName()) {
            case 'vkontakte':
                $oUser = self::findByVkId($service->getId());
                break;

            default:
                return self::ERROR_UNDEFINED_OAUTH;
        }

        if (empty($oUser)) return self::ERROR_USER_NOT_FOUND;

        return $oUser;
    }
}
