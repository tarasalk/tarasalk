<?php

namespace app\models\activeRecord;

use app\models\TaskGroup;
use OAuth\Common\Exception\Exception;
use yii\db\ActiveRecord;


class Task extends ActiveRecord {

    CONST STATUS_ACTUAL = 0; // надо выполнять
    CONST STATUS_CLOSE = 1; // задача выполнена
    CONST STATUS_ARCHIVE = -1; // в архиве

    CONST PERIOD_TYPE_DEFAULT = 0; // обычная задача
    CONST PERIOD_TYPE_PERIOD = 1; // периодическая

    public static function tableName() {
        return 'task';
    }

    public static function getGroupedTasks() {
        $aTasks = self::findAll(['status' => self::STATUS_ACTUAL]);

        $aGroupedTasks = [
            TaskGroup::GROUP_FREE       => new TaskGroup(TaskGroup::GROUP_FREE),
            TaskGroup::GROUP_YEAR       => new TaskGroup(TaskGroup::GROUP_YEAR),
            TaskGroup::GROUP_MONTH      => new TaskGroup(TaskGroup::GROUP_MONTH),
            TaskGroup::GROUP_WEEK       => new TaskGroup(TaskGroup::GROUP_WEEK),
            TaskGroup::GROUP_DAY        => new TaskGroup(TaskGroup::GROUP_DAY),
            TaskGroup::GROUP_EXPIRED    => new TaskGroup(TaskGroup::GROUP_EXPIRED),
        ];

        foreach ($aTasks as $aTask) {
            $group = TaskGroup::checkTaskGroup($aTask);

            if ($group === false) {
                //throw new Exception('undefined task group');
                continue;
            }

            if (isset($aGroupedTasks[$group])) {
                $aGroupedTasks[$group]->addTask($aTask->toArray());
            }
        }

        return $aGroupedTasks;
    }
}



