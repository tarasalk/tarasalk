<?php

use yii\db\Migration;

class m160919_060939_create_user_oauth extends Migration
{
    public function up()
    {
        $this->createTable('user_oauth', [
            'user_id' => $this->primaryKey()->notNull()->unique(),
            'vk_id' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        echo "m160919_060939_create_user_oauth cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
