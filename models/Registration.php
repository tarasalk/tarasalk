<?php

namespace app\models;

use app\models\activeRecord\UserOauth;
use nodge\eauth\ServiceBase;

class Registration {

    protected $vk_id;
    protected $firstName;
    protected $lastName;
    protected $middleName;

    public function __construct(ServiceBase $service) {
        $vk_id = $service->getId();

        $username = explode(' ', $service->getAttribute('name'));
        $firstName = $username[0];
        $lastName = $username[1];

        $this->vk_id = $vk_id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->middleName = '';
    }

    /**
     * сохраняем пользователя в бд
     * @return bool
     */
    public function save() {
        $oUser = $this->saveUser();

        if (is_null($oUser)) return false;

        $this->saveUserOauth($oUser);

        return true;
    }

    private function saveUser() {
        $oUser = new User;
        $oUser->first_name = $this->firstName;
        $oUser->last_name = $this->lastName;
        $oUser->middle_name = $this->middleName;

        if ($oUser->save())
            return $oUser;
        else
            return null;
    }

    private function saveUserOauth(User $oUser) {
        $oUserOauth = new UserOauth;
        $oUserOauth->user_id = $oUser->primaryKey;
        $oUserOauth->vk_id = $this->vk_id;

        $oUserOauth->save();
    }
}
