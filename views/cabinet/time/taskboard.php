<?php

/* @var $this yii\web\View */
/* @var $aGroupedTasks array */
/* @var $period \app\models\TaskGroup */

?>

<script>
    var addDeadline;
    var editDeadline;

    var flagAddPeriod = false;
    
    $(function() {
        date = new Date();
        date.setHours(23);
        date.setMinutes(59);

        data = {
            timepicker: true,
            dateFormat: 'yyyy-mm-dd',
            timeFormat: 'hh-ii-00',
            startDate: date
        };

        addDeadline = $('#addDateDeadline').datepicker(data).data('datepicker');
        editDeadline = $('#editDateDeadline').datepicker(data).data('datepicker');

        $('#atmCbPeriod').click(function() {
            var modal = $('#addTaskModal');

            if (flagAddPeriod) {
                modal.find('div.period').hide();
                modal.find('div.deadline').show();
            }
            else {
                modal.find('div.deadline').hide();
                modal.find('div.period').show();
            }

            flagAddPeriod = !flagAddPeriod;
        });
    });

    function showAddTaskModal() {
        var modal = $('#addTaskModal');

        modal.find('input[name="title"]').val('');
        modal.find('textarea[name="description"]').val('');
        modal.find('input[name="dateDeadline"]').val('0000-00-00 00:00');

        modal.show();
        modal.find('input[name="title"]').first().focus();
    }

    function addTask() {
        var modal = $('#addTaskModal');
        var data = {
            title: modal.find('input[name="title"]').val(),
            description: modal.find('textarea[name="description"]').val(),
        };

        if (data.title == '') {
            return toastr.warning('Нужен заголовок');
        }

        // если периодическая
        if (modal.find('input[name="periodType"]').prop('checked')) {
            data.periodType = 1;
            data.periodTime = modal.find('input[name="periodTime"]').val();

            if (!$.isNumeric(data.periodTime)) {
                return toastr.warning('Введите период (кол-во дней)');
            }
        }
        else {
            data.periodType = 0;
            data.dateDeadline = modal.find('input[name="dateDeadline"]').val();
        }

        $.getJSON({
            url: '/cabinet/time/addtask',
            type: "POST",
            data: data,
            success: function(r) {
                if (r.status == 'ok') {
                    toastr.success('Успешно');

                    setTimeout(function () {location.reload()}, 1000);
                }
                else {
                    toastr.error('Ошибка');
                }
            }
        });
    }

    function showTask(taskId) {
        $.getJSON({
            url: '/cabinet/time/gettask',
            type: "POST",
            data: {taskId: taskId},
            success: function(r) {
                if (r.status == 'ok') {
                    var modal = $('#editTaskModal');
                    var task = r.content;

                    modal.find('input[name="id"]').val(task.id);
                    modal.find('input[name="title"]').val(task.title);
                    modal.find('textarea[name="description"]').val(task.description);

                    if (task.period_type == 1) {
                        modal.find('.deadline').hide();
                        modal.find('.period').show();
                        modal.find('.repeatTask').show();
                        modal.find('input[name="periodTime"]').val(task.period_time);
                    }
                    else {
                        modal.find('.deadline').show();
                        modal.find('.period').hide();
                        modal.find('.repeatTask').hide();
                        modal.find('input[name="dateDeadline"]').val(task.date_deadline);

                        if (task.date_deadline != '0000-00-00 00:00:00') {
                            editDeadline.selectDate(new Date(task.date_deadline));
                        }
                    }

                    modal.show();
                    modal.find('input[name="title"]').first().focus();
                }
                else {
                    toastr.error('Ошибка');
                }
            }
        });
    }

    function updateTask() {
        var modal = $('#editTaskModal');
        var data = {
            taskId: modal.find('input[name="id"]').val(),
            title: modal.find('input[name="title"]').val(),
            description: modal.find('textarea[name="description"]').val(),
            dateDeadline: modal.find('input[name="dateDeadline"]').val()
        };

        if (data.title == '') {
            return toastr.warning('Нужен заголовок');
        }

        $.getJSON({
            url: '/cabinet/time/updatetask',
            type: "POST",
            data: data,
            success: function(r) {
                if (r.status == 'ok') {
                    toastr.success('Успешно');

                    setTimeout(function () {location.reload()}, 1000);
                }
                else {
                    toastr.error('Ошибка');
                }
            }
        });
    }

    function showConfirmDeleteTask() {
        taskId =  $('#editTaskModal').find('input[name="id"]').val();

        var modal = $('#confirmDeleteTaskModal');

        modal.show();
        modal.find('input[name="id"]').val(taskId);
    }

    function deleteTask() {
        var modal = $('#confirmDeleteTaskModal');
        var taskId = modal.find('input[name="id"]').val();

        $.getJSON({
            url: '/cabinet/time/deletetask',
            type: "POST",
            data: {taskId: taskId},
            success: function(r) {
                if (r.status == 'ok') {
                    toastr.success('Успешно');

                    modal.hide();
                    $('#editTaskModal').hide();

                    removeTask(taskId);
                }
                else {
                    toastr.error('Ошибка');
                }
            }
        });
    }

    function archivingTask() {
        var modal = $('#editTaskModal');
        var taskId = modal.find('input[name="id"]').val();

        $.getJSON({
            url: '/cabinet/time/archivingtask',
            type: "POST",
            data: {taskId: taskId},
            success: function(r) {
                if (r.status == 'ok') {
                    toastr.success('Успешно');
                    modal.hide();
                    removeTask(taskId);
                }
                else {
                    toastr.error('Ошибка');
                }
            }
        });
    }

    function removeTask(taskId) {
        $('#task' + taskId).remove();
    }

    function closeTask() {
        var modal = $('#editTaskModal');
        var taskId = modal.find('input[name="id"]').val();

        $.getJSON({
            url: '/cabinet/time/closetask',
            type: "POST",
            data: {taskId: taskId},
            success: function(r) {
                if (r.status == 'ok') {
                    toastr.success('Успешно');
                    modal.hide();
                    removeTask(taskId);
                }
                else {
                    toastr.error('Ошибка');
                }
            }
        });
    }

    function repeatTask() {
        var modal = $('#editTaskModal');
        var taskId = modal.find('input[name="id"]').val();

        $.getJSON({
            url: '/cabinet/time/repeattask',
            type: "POST",
            data: {taskId: taskId},
            success: function(r) {
                if (r.status == 'ok') {
                    toastr.success('Успешно');

                    setTimeout(function () {location.reload()}, 1000);
                }
                else {
                    toastr.error('Ошибка');
                }
            }
        });
    }
</script>

<div class="taskboard-container">
    <div class="taskboard">
        <? foreach ($aGroupedTasks as $period) { ?>
            <div class="column">
                <div class="title"><?=$period->getTitle()?></div>

                <? foreach ($period->getTasks() as $task) { ?>
                    <div class="item" id='task<?=$task['id']?>' onclick="showTask('<?=$task['id']?>')"><?=$task['title']?></div>
                <? } ?>

                <div class="addItem" onclick="showAddTaskModal()">Добавить карточку</div>
            </div>
        <? } ?>
    </div>

    <div id="addTaskModal" class="modal-overlay" onclick="closeModal(this, event);">
        <div class="modal-window">
            <button class="window-close" onclick="closeModal(this);">&times;</button>
            <div class="window-header border-blue">Добавить карточку</div>
            <div class="window-body">
                <div class="row title">
                    <input name='title' type='text' placeholder='Заголовок'>
                </div>
                <div class="row description">
                    <textarea name='description' placeholder="Описание"></textarea>
                </div>
                <div class="row">
                    <label for="atmCbPeriod"><input type="checkbox" name="periodType" id="atmCbPeriod"> периодическая</label>
                </div>
                <div class="row deadline">
                    Дата дедлайна <input type='text' id='addDateDeadline' name="dateDeadline" value="0000-00-00 00-00-00" readonly>
                </div>
                <div class="row period">
                    Период: <input name='periodTime' type='text' placeholder='Кол-во дней'>
                </div>
                <div class="window-footer">
                    <button class="tbtn btn-middle blue" onclick="addTask()">Добавить</button>
                </div>
            </div>
        </div>
    </div>

    <div id="editTaskModal" class="modal-overlay" onclick="closeModal(this, event);">
        <div class="modal-window">
            <button class="window-close" onclick="closeModal(this);">&times;</button>
            <div class="window-header border-blue">Просмотр задачи</div>
            <div class="window-body">
                <input name='id' type="hidden">
                <div class="row title">
                    <input name='title' type='text' placeholder='Заголовок'>
                </div>
                <div class="row description">
                    <textarea name='description' placeholder="Описание"></textarea>
                </div>
                <div class="row deadline">
                    Дата дедлайна <input type='text' id='editDateDeadline' name="dateDeadline" value="0000-00-00 00-00-00" readonly>
                </div>
                <div class="row period">
                    Период: <input name='periodTime' type='text' placeholder='Кол-во дней'>
                </div>
                <div class="row">
                    <button class="tbtn btn-middle green" onclick="closeTask()">Завершить</button>
                </div>
                <div class="row repeatTask">
                    <button class="tbtn btn-middle green" onclick="repeatTask()">Завершить и повторить</button>
                </div>
                <div class="row">
                    <button class="tbtn btn-middle red" onclick="archivingTask()">Архив</button>
                </div>
                <div class="row">
                    <button class="tbtn btn-middle red" onclick="showConfirmDeleteTask()">Удалить</button>
                </div>
                <div class="window-footer">
                    <button class="tbtn btn-middle blue" onclick="updateTask()">Сохранить</button>
                </div>
            </div>
        </div>
    </div>

    <div id="confirmDeleteTaskModal" class="modal-overlay" onclick="closeModal(this, event);">
        <div class="modal-window">
            <button class="window-close" onclick="closeModal(this);">&times;</button>
            <div class="window-header border-red">Подтверждение удаления</div>
            <div class="window-body">
                <input name='id' type="hidden">
                <div class="window-footer">
                    <button class="tbtn btn-middle red" onclick="deleteTask()">Удалить</button>
                    <button class="tbtn btn-middle blue" onclick="closeModal(this);">Отмена</button>
                </div>
            </div>
        </div>
    </div>
</div>