<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\CabinetAsset;
use app\components\widgets\cabinet\MainMenu\Menu;


CabinetAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="cabinet">
        <div class="sidebar"><?= Menu::widget([]) ?></div>
        <div class="right-container">
            <div class="header">
                <div>
                    <div>
                        <div>Привет <?=Yii::$app->user->identity->first_name?>!</div>
                    </div>
                    <div>
                        <a href="/">Главная</a>
                        <a href="/site/logout">Выйти</a>
                    </div>
                </div>
            </div>
            <div class="title"><?= isset($this->title) ? $this->title : 'Кабинет' ?></div>
            <div class="content">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
