<?php

namespace app\components\widgets\cabinet\MainMenu;

use yii;
use yii\base\Widget;

class Menu extends Widget {

    public function run() {
        $aMenu = [
            [
                'title' => 'Рабочий стол',
                'link' => '/cabinet/user/desktop',
                'class' => 'desktop'
            ],
            [
                'title' => 'Тайм менеджмент',
                'link' => '/cabinet/time/taskboard',
                'class' => 'timemanagement',
                'level2' => [
                    [
                        'title' => 'Доска задач',
                        'link' => '/cabinet/time/taskboard'
                    ],
                    /*[
                        'title' => 'Архив',
                        'link' => '/cabinet/time/archiv'
                    ]*/
                ]
            ],
            /*[
                'title' => 'Спорт',
                'link' => '/cabinet/user/sport',
                'class' => '',
                'level2' => [
                    [
                        'title' => 'упражнения',
                        'link' => '/cabinet/sport/exercise'
                    ],
                    [
                        'title' => 'Архив',
                        'link' => '/cabinet/sport/archiv'
                    ]
                ]
            ],*/
        ];

        return $this->render('index', compact('aMenu'));
    }
}
