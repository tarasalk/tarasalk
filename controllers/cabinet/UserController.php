<?php

namespace app\controllers\cabinet;

use yii;

class UserController extends CabinetController {

    public function actionDesktop() {
        $this->view->title = 'Рабочий стол';
        
        return $this->render('desktop');
    }
}
